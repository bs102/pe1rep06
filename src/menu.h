#pragma once

typedef struct menu{
  int id;
  char *name;
  int (*func)();
  int keycode;
  int trigger_id;
  int next_id;
  struct menu *next;
} menu_t;

int add_menu_entry(menu_t **menu, char *name, int (*f)(), int keycode, int trigger_id, int menu_id);
int get_menu_entry(menu_t *menu, int menu_id, menu_t **out);
int get_menu_entries(menu_t *menu, int trigger_id, menu_t **out);
int clear_menu(menu_t *menu);
