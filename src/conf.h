#pragma once

typedef struct conf{
  char *name;
  int val;
  int constant;
  int min;
  int max;
  struct conf *next;
} conf_t;

int add_conf_entry(conf_t **conf, char *name, int val, int constant, int min, int max);
int get_conf_entry(conf_t *conf, char *name, conf_t **out);
int get_conf_entry_value(conf_t *conf, char *name, int *out);
int get_conf_entries(conf_t *conf, conf_t **out);
int set_conf_entry(conf_t *conf, char *name, int val);
int clear_conf(conf_t *conf);
