#include "common.h"

#include <string.h>
#include <assert.h>

#include "menu.h"

int add_menu_entry(menu_t **menu, char *name, int (*f)(), int keycode, int trigger_id, int menu_id){
  menu_t *nmenu, *mp;
  int latest_id = 0;
  if ((nmenu = malloc(sizeof(menu_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  for (mp = *menu; mp != NULL; mp = mp->next) latest_id = mp->id;
  nmenu->id = ++latest_id;
  nmenu->name = name;
  nmenu->func = f;
  nmenu->keycode = keycode;
  nmenu->trigger_id = trigger_id;
  nmenu->next_id = menu_id;
  nmenu->next = NULL;
  if (*menu == NULL) *menu = nmenu;
  else{
    menu_t *mp, *mpb;
    for (mp = *menu; mp != NULL; mp = mp->next){
      if (strcmp(name, mp->name) == 0 && trigger_id == mp->trigger_id){
        free(nmenu);
        return PROG_SUCCESS;
      }
      mpb = mp;
    }
    mpb->next = nmenu;
  }
  return PROG_SUCCESS;
}

int get_menu_entry(menu_t *menu, int menu_id, menu_t **out){
  if (menu == NULL) return PROG_ERR_ALREADY_FREED;
  if ((*out = malloc(sizeof(menu_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  menu_t *mp;
  for (mp = menu; mp != NULL; mp = mp->next){
    if (mp->id == menu_id){
      memmove(*out, mp, sizeof(menu_t));
      (*out)->next = NULL;
      return PROG_SUCCESS;
    }
  }
  free(*out);
  return PROG_ERR_INVALID_CASE;
}

int get_menu_entries(menu_t *menu, int trigger_id, menu_t **out){
  if (menu == NULL) return PROG_ERR_ALREADY_FREED;
  menu_t *mp;
  for (mp = menu; mp != NULL; mp = mp->next){
    if (mp->trigger_id == trigger_id){
      int ret;
      if (*out != NULL){
        menu_t *entry, *nentry = NULL;
        for (entry = *out; entry->next != NULL; entry = entry->next);
        if ((ret = get_menu_entry(menu, mp->id, &nentry)) != PROG_SUCCESS) return ret;
        entry->next = nentry;
      }else{
        if ((ret = get_menu_entry(menu, mp->id, out)) != PROG_SUCCESS) return ret;
      }
    }
  }
  return PROG_SUCCESS;
}

int clear_menu(menu_t *menu){
  if (menu == NULL) return PROG_ERR_ALREADY_FREED;
  while(menu != NULL){
    menu_t *next = menu->next;
    free(menu);
    menu = next;
  }
  return PROG_SUCCESS;
}
