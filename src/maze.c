#include "common.h"

#include <assert.h>
#include <malloc.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>

#include "maze.h"
#include "util.h"

static int get_cluster_idx(maze_t *maze, int x, int y){
  int i;
  for (i = y*maze->width+x; i != maze->room[i]; i = maze->room[i]);
  return i;
}

static void connect_cluster(maze_t *maze, int x1, int y1, int x2, int y2){
  assert(x1 >= 0 && y1 >= 0 && x2 >= 0 && y2 >= 0);
  int i1 = get_cluster_idx(maze, x1, y1), i2 = get_cluster_idx(maze, x2, y2);
  if (i1 < i2) maze->room[i2] = i1;
  else maze->room[i1] = i2;
}

static int check_all_same_cluster(maze_t *maze){
  int x, y, d = get_cluster_idx(maze, 0, 0);
  for (x = 0; x < maze->width; ++x){
    for (y = 0; y < maze->height; ++y){
      if (d != get_cluster_idx(maze, x, y)) return 0;
    }
  }
  return 1;
}

int exists_wall(maze_t *maze, int x1, int y1, int x2, int y2){
  if (x1 != x2){
    if (x1 < x2) return maze->wall[x1+(y1*2)*maze->width];
    else return maze->wall[x2+(y1*2)*maze->width];
  }else{
    if (y1 < y2) return maze->wall[x1+(y1*2+1)*maze->width];
    else return maze->wall[x1+(y2*2+1)*maze->width];
  }
}

static int make_maze_run(maze_t *maze, int difficulty){
  int x;
  double df = difficulty / 100.0;
  direction dir[4] = {0};

  /* initialize */
  for (x = 0; x < maze->width*maze->height; ++x) maze->room[x] = x;
  for (x = 0; x < maze->width*maze->height*2; ++x) maze->wall[x] = 1;

  /* create maze */
  while (check_all_same_cluster(maze) != 1){
    int y;
    for (x = 0; x < maze->width; ++x){
      for (y = 0; y < maze->height; ++y){
        if (prog_rand() / (double)(UINT_MAX) < df) continue;
        int t = 0, b = 0, l = 0, r = 0, i = 0, rand;
        int idx = get_cluster_idx(maze, x, y);
        /* top */ if (y != 0) t = (exists_wall(maze, x, y, x, y-1) && idx != get_cluster_idx(maze, x, y-1));
        /* bottom */ if (y != maze->height-1) b = (exists_wall(maze, x, y, x, y+1) && idx != get_cluster_idx(maze, x, y+1));
        /* left */ if (x != 0) l = (exists_wall(maze, x, y, x-1, y) && idx != get_cluster_idx(maze, x-1, y));
        /* right */ if (x != maze->width-1) r = (exists_wall(maze, x, y, x+1, y) && idx != get_cluster_idx(maze, x+1, y));
        if (t+b+l+r == 0) continue;
        if (t) dir[i++] = TOP;
        if (b) dir[i++] = BOTTOM;
        if (l) dir[i++] = LEFT;
        if (r) dir[i++] = RIGHT;
        rand = prog_rand() % i;
        switch(dir[rand]){
          case TOP:
            maze->wall[x+(y*2-1)*maze->width] = 0;
            connect_cluster(maze, x, y, x, y-1);
            break;
          case BOTTOM:
            maze->wall[x+(y*2+1)*maze->width] = 0;
            connect_cluster(maze, x, y, x, y+1);
            break;
          case LEFT:
            maze->wall[x-1+(y*2)*maze->width] = 0;
            connect_cluster(maze, x, y, x-1, y);
            break;
          case RIGHT:
            maze->wall[x+(y*2)*maze->width] = 0;
            connect_cluster(maze, x, y, x+1, y);
            break;
          default:
            return PROG_ERR_INVALID_CASE;
        }
      }
    }
  }
  return PROG_SUCCESS;
}

int make_maze(maze_t **maze, int width, int height, int difficulty){
  assert(width > 0 && height > 0);
  struct timespec ts;
  if (*maze != NULL) return PROG_ERR_NOT_FREED;
  if ((*maze = malloc(sizeof(maze_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  maze_t *m = *maze;
#if __USE_ISOC11
  if (timespec_get(&ts, TIME_UTC) == 0) return PROG_ERR_FAILED_GET_TIME;
#else
  if (clock_gettime(CLOCK_REALTIME, &ts) == -1) return PROG_ERR_FAILED_GET_TIME;
#endif
  m->width = width;
  m->height = height;
  m->room = malloc(m->width*m->height * sizeof(int));
  if (m->room == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  m->wall = malloc(m->width*m->height*2 * sizeof(int));
  if (m->wall == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  m->start = 0;
  m->goal = (m->width-1)+(m->height-1)*width;

  prog_srand(ts.tv_nsec ^ ts.tv_sec);

  return make_maze_run(*maze, difficulty);
}

int clear_maze(maze_t *maze){
  if (maze->room == NULL) return PROG_ERR_ALREADY_FREED;
  free(maze->room);
  if (maze->wall == NULL) return PROG_ERR_ALREADY_FREED;
  free(maze->wall);
  maze->room = NULL;
  maze->wall = NULL;
  free(maze);
  return PROG_SUCCESS;
}
