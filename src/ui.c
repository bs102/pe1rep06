#include "common.h"

#include <curses.h>
#include <string.h>
#include <limits.h>
#include <pthread.h>
#include <time.h>

#include "maze.h"
#include "solver.h"
#include "menu.h"
#include "conf.h"
#include "ui.h"

static void initialize_core(core_t *core){
  core->path = NULL;
  core->maze = NULL;
  core->conf = NULL;
  core->menu = NULL;
  core->ui = NULL;
  core->com = NULL;
}

static void initialize_ui(ui_t *ui){
  ui->current_id = -1;
  ui->current_func_id = 1;
  ui->extra = 0;
  ui->thread = NULL;

  /* curses */
  initscr();
  noecho();
  cbreak();
  keypad(stdscr, true);
  curs_set(0);
  refresh();

  /* color */
  start_color();
  init_pair(1, COLOR_WHITE, COLOR_BLACK);
  init_pair(2, COLOR_CYAN, COLOR_BLACK);  /* user */
  init_pair(3, COLOR_YELLOW, COLOR_BLACK); /* com */
}

static void clear_ui(void){
  clear();
  refresh();
  endwin();
}

static void kill_thread(ui_t *ui){
  pthread_cancel(*ui->thread);
  pthread_join(*ui->thread, NULL);
  free(ui->thread);
  ui->thread = NULL;
}

static void draw_maze(WINDOW *win, maze_t *maze, path_t *path, conf_t *conf){
  int x, y, x_offset, y_offset, x_repeat, width, height;
  get_conf_entry_value(conf, "x_offset", &x_offset);
  get_conf_entry_value(conf, "y_offset", &y_offset);
  get_conf_entry_value(conf, "x_repeat", &x_repeat);
  get_conf_entry_value(conf, "width", &width);
  get_conf_entry_value(conf, "height", &height);
  for (x = 0; x < width; ++x){
    for (y = 0; y < height; ++y){
      path_t *p;
      if (x != width-1 && exists_wall(maze, x, y, x+1, y)) mvwaddch(win, y*2+1+y_offset, (x+1)*x_repeat+x_offset, '|');
      if (y != height-1){
        if (exists_wall(maze, x, y, x, y+1)) mvwaddstr(win, y*2+2+y_offset, x*x_repeat+1+x_offset, "----");
        else mvwaddstr(win, y*2+2+y_offset, x*x_repeat+1+x_offset, "   -");
      }
      if ((x == 0 && y != 0) || (x == width-1 && y != height-1)){
        mvwaddch(win, y*2+1+y_offset, ((!x)?x:(x+1)*x_repeat)+x_offset, '|');
        mvwaddch(win, y*2+2+y_offset, ((!x)?x:(x+1)*x_repeat)+x_offset, '-');
      }
      if (y == 0 || y == height-1){
        mvwaddstr(win, ((!y)?y:y*2+2)+y_offset, x*x_repeat+x_offset, "----");
        if (x == 0 || x == width-1) mvwaddch(win, ((!y)?y:y*2+2)+y_offset, ((!x)?x:(x+1)*x_repeat)+x_offset, '+');
      }
      for (p = path; p != NULL; p = p->next){
        if (p->room == &maze->room[x+width*y]){
          attron(COLOR_PAIR(3));
          mvwaddstr(win, y*2+1+y_offset, x*x_repeat+1+x_offset, " * ");
          attroff(COLOR_PAIR(3));
          break;
        }
      }
      if (p == NULL) mvwaddstr(win, y*2+1+y_offset, x*x_repeat+1+x_offset, "   ");
    }
  }
}

static void draw_cursor(WINDOW *win, core_t *core){
  maze_t *maze = core->maze;
  ui_t *ui = core->ui;
  int x, y, x_offset, y_offset, x_repeat, width;
  get_conf_entry_value(core->conf, "x_offset", &x_offset);
  get_conf_entry_value(core->conf, "y_offset", &y_offset);
  get_conf_entry_value(core->conf, "x_repeat", &x_repeat);
  get_conf_entry_value(core->conf, "width", &width);
  if (ui->extra != 0){
    x = ui->extra % maze->width; y = (ui->extra - x) / maze->width;
  }else{
    x = 0; y = 0;
  }
  attron(COLOR_PAIR(2));
  mvwaddstr(win, y*2+1+y_offset, x*x_repeat+1+x_offset, " * ");
  attroff(COLOR_PAIR(2));
}

static int run_chg_setting_edit(core_t *core){
  menu_t *mp = NULL;
  conf_t *cp = NULL;
  ui_t *ui = core->ui;
  int ret, x_offset, y_offset;
  get_conf_entry_value(core->conf, "x_offset", &x_offset);
  get_conf_entry_value(core->conf, "y_offset", &y_offset);
  if ((ret = get_menu_entry(core->menu, ui->current_func_id, &mp)) != PROG_SUCCESS) return ret;
  ui->extra = ui->current_func_id;
  if ((ret = get_conf_entry(core->conf, mp->name, &cp)) != PROG_SUCCESS) return ret;
  mvprintw(y_offset+(mp->keycode-'0'), x_offset, "  %s\t= %d\t[%d-%d]", mp->name, cp->val, cp->min, cp->max);
  free(mp);
  free(cp);
  return PROG_SUCCESS;
}

static int run_chg_setting_up(core_t *core){
  menu_t *mp = NULL;
  conf_t *cp = NULL;
  ui_t *ui = core->ui;
  int ret;
  if ((ret = get_menu_entry(core->menu, ui->extra, &mp)) != PROG_SUCCESS) return ret;
  if ((ret = get_conf_entry(core->conf, mp->name, &cp)) != PROG_SUCCESS) return ret;
  if (cp->val < cp->max) if ((ret = set_conf_entry(core->conf, mp->name, ++cp->val)) != PROG_SUCCESS) return ret;
  ui->current_func_id = ui->extra;
  free(mp);
  free(cp);
  return run_chg_setting_edit(core);
}

static int run_chg_setting_down(core_t *core){
  menu_t *mp = NULL;
  conf_t *cp = NULL;
  ui_t *ui = core->ui;
  int ret;
  if ((ret = get_menu_entry(core->menu, ui->extra, &mp)) != PROG_SUCCESS) return ret;
  if ((ret = get_conf_entry(core->conf, mp->name, &cp)) != PROG_SUCCESS) return ret;
  if (cp->val > cp->min) if ((ret = set_conf_entry(core->conf, mp->name, --cp->val)) != PROG_SUCCESS) return ret;
  ui->current_func_id = ui->extra;
  free(mp);
  free(cp);
  return run_chg_setting_edit(core);
}

static int run_chg_setting(core_t *core){
  conf_t *cp;
  int i = 1, x_offset, y_offset;
  get_conf_entry_value(core->conf, "x_offset", &x_offset);
  get_conf_entry_value(core->conf, "y_offset", &y_offset);
  mvprintw(y_offset, x_offset, "You can change these variables:");
  for (cp = core->conf; cp != NULL; cp = cp->next, ++i){
    if (cp->constant) continue;
    int ret;
    if ((ret = add_menu_entry((menu_t**)&core->menu, cp->name, &run_chg_setting_edit, '0'+i, 2, 3)) != PROG_SUCCESS) return ret;
    mvprintw(y_offset+i, x_offset, "  %s\t= %d\t[%d-%d]", cp->name, cp->val, cp->min, cp->max);
  }
  return PROG_SUCCESS;
}

static int run_maze(core_t *core){
  int ret, width, height, difficulty;
  ui_t *ui = core->ui;
  get_conf_entry_value(core->conf, "width", &width);
  get_conf_entry_value(core->conf, "height", &height);
  get_conf_entry_value(core->conf, "rate", &difficulty);
  ui->extra = 0;
  if (core->maze != NULL){
    if ((ret = clear_maze(core->maze)) != PROG_SUCCESS) return ret;
    core->maze = NULL;
  }
  if (core->path != NULL){
    if ((ret = clear_path_list(core->path)) != PROG_SUCCESS) return ret;
    core->path = NULL;
  }
  if (core->com != NULL){
    com_t *com = core->com;
    if ((ret = clear_path_list(com->path)) != PROG_SUCCESS) return ret;
    free(com->read);
    free(com);
    core->com = NULL;
  }
  if ((ret = make_maze((maze_t**)&core->maze, width, height, difficulty)) != PROG_SUCCESS) return ret;
  draw_maze(stdscr, core->maze, NULL, core->conf);
  draw_cursor(stdscr, core);
  return PROG_SUCCESS;
}

static int run_maze_vs_com_draw(WINDOW *win, void *core){
  maze_t *maze = ((core_t*)core)->maze;
  com_t *com = ((core_t*)core)->com;
  conf_t *conf = ((core_t*)core)->conf;
  draw_maze(win, maze, com->path, conf);
  draw_cursor(win, core);
  wrefresh(win);
  return PROG_SUCCESS;
}

static void *run_maze_vs_com(void *core){
  maze_t *maze = ((core_t*)core)->maze;
  com_t *com = ((core_t*)core)->com;
  conf_t *conf = ((core_t*)core)->conf;
  int strength;
  struct timespec ts;
  get_conf_entry_value(conf, "strength", &strength);
  strength = 1000 - strength; /* ms */
  ts.tv_sec = strength / 1000;
  ts.tv_nsec = (strength % 1000) * 1000000;
  while (com->tpath->room != &maze->room[maze->goal]){
    if (run_solver_step(maze, com) != PROG_SUCCESS) pthread_exit(NULL);
    use_window(stdscr, run_maze_vs_com_draw, core);
    nanosleep(&ts, NULL);
  }
  pthread_exit(NULL);
}

static int run_maze_vs_user(core_t *core){
  struct timespec ts;
  int ret, width, height, difficulty;
  ui_t *ui = core->ui;
  get_conf_entry_value(core->conf, "width", &width);
  get_conf_entry_value(core->conf, "height", &height);
  get_conf_entry_value(core->conf, "rate", &difficulty);
  ui->extra = 0;
  /* kill thread */
  if (ui->thread != NULL) kill_thread(ui);
  if (core->maze != NULL){
    if ((ret = clear_maze(core->maze)) != PROG_SUCCESS) return ret;
    core->maze = NULL;
  }
  if (core->com != NULL){
    com_t *com = core->com;
    if ((ret = clear_path_list(com->path)) != PROG_SUCCESS) return ret;
    free(com->read);
    free(com);
    core->com = NULL;
  }
  if ((ret = make_maze((maze_t**)&core->maze, width, height, difficulty)) != PROG_SUCCESS) return ret;
  maze_t *maze = core->maze;
  if (
      (core->com = malloc(sizeof(com_t))) == NULL ||
      (((com_t*)core->com)->path = malloc(sizeof(path_t))) == NULL ||
      (((com_t*)core->com)->read = calloc(maze->width * maze->height, sizeof(int))) == NULL
  ) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  com_t *com = core->com;
  com->tpath = com->path;
  com->path->room = &maze->room[maze->start];
  com->path->next = NULL;
  com->read[maze->start] = 1;
  /* create thread */
  if ((ui->thread = malloc(sizeof(pthread_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  if (pthread_create(ui->thread, NULL, &run_maze_vs_com, core) != 0) return PROG_ERR_FAILED_CREATE_THREAD;
  ts.tv_sec = 0;
  ts.tv_nsec = 10000000;
  nanosleep(&ts, NULL);
  return PROG_SUCCESS;
}

static int run_move_up(core_t *core){
  int x, y;
  maze_t *maze = core->maze;
  ui_t *ui = core->ui;
  if (ui->extra != 0){
    x = ui->extra % maze->width; y = (ui->extra - x) / maze->width;
  }else{
    x = 0; y = 0;
  }
  if (y != 0 && !exists_wall(maze, x, y, x, y-1)) --y;
  ui->extra = x + maze->width * y;
  draw_maze(stdscr, core->maze, (core->com != NULL) ? ((com_t*)core->com)->path : NULL, core->conf);
  draw_cursor(stdscr, core);
  return PROG_SUCCESS;
}

static int run_move_down(core_t *core){
  int x, y;
  maze_t *maze = core->maze;
  ui_t *ui = core->ui;
  if (ui->extra != 0){
    x = ui->extra % maze->width; y = (ui->extra - x) / maze->width;
  }else{
    x = 0; y = 0;
  }
  if (y != maze->height-1 && !exists_wall(maze, x, y, x, y+1)) ++y;
  ui->extra = x + maze->width * y;
  draw_maze(stdscr, core->maze, (core->com != NULL) ? ((com_t*)core->com)->path : NULL, core->conf);
  draw_cursor(stdscr, core);
  return PROG_SUCCESS;
}

static int run_move_left(core_t *core){
  int x, y;
  maze_t *maze = core->maze;
  ui_t *ui = core->ui;
  if (ui->extra != 0){
    x = ui->extra % maze->width; y = (ui->extra - x) / maze->width;
  }else{
    x = 0; y = 0;
  }
  if (x != 0 && !exists_wall(maze, x, y, x-1, y)) --x;
  ui->extra = x + maze->width * y;
  draw_maze(stdscr, core->maze, (core->com != NULL) ? ((com_t*)core->com)->path : NULL, core->conf);
  draw_cursor(stdscr, core);
  return PROG_SUCCESS;
}

static int run_move_right(core_t *core){
  int x, y;
  maze_t *maze = core->maze;
  ui_t *ui = core->ui;
  path_t *com_path = (core->com != NULL) ? ((com_t*)core->com)->path : NULL;
  if (ui->extra != 0){
    x = ui->extra % maze->width; y = (ui->extra - x) / maze->width;
  }else{
    x = 0; y = 0;
  }
  if (ui->extra == maze->goal) return (com_path) ? run_maze_vs_user(core) : run_maze(core);
  if (x != maze->width-1 && !exists_wall(maze, x, y, x+1, y)) ++x;
  ui->extra = x + maze->width * y;
  draw_maze(stdscr, core->maze, com_path, core->conf);
  draw_cursor(stdscr, core);
  return PROG_SUCCESS;
}

static int run_show_answer(core_t *core){
  int ret;
  if (core->path != NULL){
    if ((ret = clear_path_list(core->path)) != PROG_SUCCESS) return ret;
    core->path = NULL;
  }
  if ((ret = run_solver(core->maze, (path_t**)&core->path)) != PROG_SUCCESS) return ret;
  draw_maze(stdscr, core->maze, core->path, core->conf);
  return PROG_SUCCESS;
}

static int run_main(core_t *core){
  int row, col;
  char *prog_title = PROG_NAME;
  char *prog_version = PROG_VERSION;
  ui_t *ui = core->ui;
  if (ui->thread != NULL) kill_thread(ui);
  getmaxyx(stdscr, row, col);
  mvprintw(row/2, (col-strlen(prog_title))/2, "%s", prog_title);
  mvprintw(row/2+1, (col-strlen(prog_version))/2, "%s", prog_version);
  return PROG_SUCCESS;
}

static int run_quit(void){
  return 1;
}

static void display_header(void){
  int row, col;
  getmaxyx(stdscr, row, col);
  move(1, 0);
  hline('-', col);
}

static void display_header_title(menu_t *menu, int current_id){
  menu_t *m;
  for (m = menu; m != NULL; m = m->next){
    if (m->id == current_id){
      mvprintw(0, 0, "%s", m->name);
      return;
    }
  }
}

static void display_footer(void){
  int row, col;
  getmaxyx(stdscr, row, col);
  move(row-2, 0);
  hline('-', col);
}

static int display_footer_menu(menu_t *menu, int current_id){
  menu_t *m = NULL, *mp;
  int ret;
  int row, col;
  getmaxyx(stdscr, row, col);
  /* menu */
  if ((ret = get_menu_entries(menu, current_id, &m)) != PROG_SUCCESS) return ret;
  /* force entry (-1) */
  if ((ret = get_menu_entries(menu, -1, &m)) != PROG_SUCCESS) return ret;
  move(row-1, 0);
  for (mp = m; mp != NULL; mp = mp->next){
    int x, y;
    char keycode[6];
    switch (mp->keycode){
      case KEY_UP: strcpy(keycode, "Up"); break;
      case KEY_DOWN: strcpy(keycode, "Down"); break;
      case KEY_LEFT: strcpy(keycode, "Left"); break;
      case KEY_RIGHT: strcpy(keycode, "Right"); break;
      default: strcpy(keycode, (char[]){mp->keycode, '\0'});
    }
    getyx(stdscr, y, x);
    mvprintw(y, x, "[%s] %s ", keycode, mp->name);
  }
  clear_menu(m);
  return PROG_SUCCESS;
}

static int add_menu(menu_t **menu){
  int ret;
  if ((ret = add_menu_entry(menu, "top", &run_main, 't', -1, 0)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "quit", &run_quit, 'q', -1, 0)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "play", &run_maze, 'p', 0, 1)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "vs play", &run_maze_vs_user, 'v', 0, 5)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "change settings", &run_chg_setting, 'c', 0, 2)) != PROG_SUCCESS) return ret; 
  if ((ret = add_menu_entry(menu, "back", &run_chg_setting, 'b', 3, 2)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "+1", &run_chg_setting_up, KEY_UP, 3, 3)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "-1", &run_chg_setting_down, KEY_DOWN, 3, 3)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "move up", &run_move_up, KEY_UP, 1, 1)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "move down", &run_move_down, KEY_DOWN, 1, 1)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "move left", &run_move_left, KEY_LEFT, 1, 1)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "move right", &run_move_right, KEY_RIGHT, 1, 1)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "reset", &run_maze, 'r', 1, 1)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "show answer", &run_show_answer, 'a', 1, 4)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "reset", &run_maze, 'r', 4, 1)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "move up", &run_move_up, KEY_UP, 5, 5)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "move down", &run_move_down, KEY_DOWN, 5, 5)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "move left", &run_move_left, KEY_LEFT, 5, 5)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "move right", &run_move_right, KEY_RIGHT, 5, 5)) != PROG_SUCCESS) return ret;
  if ((ret = add_menu_entry(menu, "reset", &run_maze_vs_user, 'r', 5, 5)) != PROG_SUCCESS) return ret;
  return PROG_SUCCESS;
}

static int add_conf(conf_t **conf){
  int ret;
  if ((ret = add_conf_entry(conf, "width", 30, 0, 1, INT_MAX)) != PROG_SUCCESS) return ret;
  if ((ret = add_conf_entry(conf, "height", 18, 0, 1, INT_MAX)) != PROG_SUCCESS) return ret;
  if ((ret = add_conf_entry(conf, "rate", 99, 0, 1, 99)) != PROG_SUCCESS) return ret;
  if ((ret = add_conf_entry(conf, "strength", 100, 0, 1, 1000)) != PROG_SUCCESS) return ret;
  if ((ret = add_conf_entry(conf, "x_offset", 2, 1, 0, 0)) != PROG_SUCCESS) return ret;
  if ((ret = add_conf_entry(conf, "y_offset", 2, 1, 0, 0)) != PROG_SUCCESS) return ret;
  if ((ret = add_conf_entry(conf, "x_repeat", 4, 1, 0, 0)) != PROG_SUCCESS) return ret;
  return PROG_SUCCESS;
}

static int run_func(int keycode, core_t *core, int current_id){
  menu_t *mp;
  for (mp = core->menu; mp != NULL; mp = mp->next){
    if (keycode == mp->keycode && (mp->trigger_id == -1 || current_id == mp->trigger_id)){
      ui_t *ui = core->ui;
      erase();
      int ret;
      ui->current_id = mp->next_id;
      ui->current_func_id = mp->id;
      if ((ret = mp->func(core)) != PROG_SUCCESS) return ret;
      return PROG_SUCCESS;
    }
  }
  return PROG_SUCCESS;
}

void run(void){
  int ret, initial = 1;
  core_t *core;
  ui_t *ui;
  if ((core = malloc(sizeof(core_t))) == NULL){
    ret = PROG_ERR_FAILED_ALLOCATE_MEMORY;
    goto FAIL;
  }
  initialize_core(core);

  if ((core->ui = malloc(sizeof(ui_t))) == NULL){
    ret = PROG_ERR_FAILED_ALLOCATE_MEMORY;
    goto FAIL;
  }
  ui = core->ui;

  if ((ret = add_conf((conf_t**)&core->conf)) != PROG_SUCCESS) goto FAIL;
  if ((ret = add_menu((menu_t**)&core->menu)) != PROG_SUCCESS) goto FAIL;

  initialize_ui(core->ui);
  while(1){
    int keycode;
    ui_t *ui = core->ui;
    if (initial) keycode = 't';
    else keycode = wgetch(stdscr);
    if ((ret = run_func(keycode, core, ui->current_id)) != PROG_SUCCESS){
      if (ret != 1) goto FAIL;
      else break;
    }
    display_header();
    display_footer();
    display_header_title(core->menu, ui->current_func_id);
    if ((ret = display_footer_menu(core->menu, ui->current_id)) != PROG_SUCCESS) goto FAIL;
    refresh();
    initial = 0;
  }
  if (ui->thread != NULL) kill_thread(ui);
  if (core->maze != NULL) clear_maze(core->maze);
  if (core->path != NULL) clear_path_list(core->path);
  if (core->com != NULL){
    clear_path_list(((com_t*)core->com)->path);
    free(((com_t*)core->com)->read);
    free(core->com);
  }
  clear_conf(core->conf);
  clear_menu(core->menu);
  free(core->ui);
  free(core);
  clear_ui();

  return;
FAIL:
  clear_ui();
  fprintf(stderr, "an unexpected error has occurred (code = %d", ret);
  if (errno != 0) fprintf(stderr, ", errno = %d", errno);
  fprintf(stderr, ")\n");
  exit(EXIT_FAILURE);
}
