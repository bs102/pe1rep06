#pragma once

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

/* status code */
enum{
  PROG_SUCCESS                    = 0, /* success */
  PROG_ERR_FAILED_ALLOCATE_MEMORY = -1, /* failed to allocate memory */
  PROG_ERR_NOT_FREED              = -2, /* needs to free resource */
  PROG_ERR_ALREADY_FREED          = -3, /* already freed */
  PROG_ERR_FAILED_GET_TIME        = -4, /* failed to get time */
  PROG_ERR_INVALID_CASE           = -5, /* invalid case */
  PROG_ERR_FAILED_CREATE_THREAD   = -6, /* failed to create thread */
  PROG_ERR_UNKNOWN                = -99, /* unknown error */
};

/* core */
typedef struct{
  void *maze;
  void *path;
  void *conf;
  void *menu;
  void *ui;
  void *com;
} core_t;
