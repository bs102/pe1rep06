#pragma once

typedef struct path{
  int *room;
  struct path *next;
} path_t;

typedef struct{
  path_t *path;
  path_t *tpath;
  int *read;
} com_t;

int run_solver(maze_t *maze, path_t **path);
int run_solver_step(maze_t *maze, com_t *com);
int clear_path_list(path_t *path);
