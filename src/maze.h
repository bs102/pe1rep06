#pragma once

typedef enum{
  TOP,
  BOTTOM,
  LEFT,
  RIGHT
} direction;

typedef struct{
  int width;
  int height;
  int start;
  int goal;
  int *room;
  int *wall;
} maze_t;

int exists_wall(maze_t *maze, int x1, int y1, int x2, int y2);
int make_maze(maze_t **maze, int width, int height, int difficulty);
int clear_maze(maze_t *maze);
