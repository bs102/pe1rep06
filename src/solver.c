#include "common.h"

#include <malloc.h>

#include "maze.h"
#include "solver.h"

static int set_read_flag(maze_t *maze, int *read, int *room, int flag){
  int i;
  for (i = 0; i < maze->width*maze->height; ++i){
    if (room == &maze->room[i]){
      read[i] = flag;
      return PROG_SUCCESS;
    }
  }
  return PROG_ERR_UNKNOWN;
}

static int get_read_flag(maze_t *maze, int *read, int *room, int *out){
  int i;
  for (i = 0; i < maze->width*maze->height; ++i){
    if (room == &maze->room[i]){
      *out = read[i];
      return PROG_SUCCESS;
    }
  }
  return PROG_ERR_UNKNOWN;
}

static int pop_path_list(path_t **path){
  path_t *p = *path;
  if (p == NULL || (p->room == NULL && p->next == NULL)) return PROG_ERR_ALREADY_FREED;
  path_t *next = p->next;
  free(p);
  *path = next;
  return PROG_SUCCESS;
}

static int push_path_list(path_t **path, int *room){
  if (path == NULL || *path == NULL) return PROG_ERR_ALREADY_FREED;
  path_t *p = *path;
  if (p->room == NULL && p->next == NULL){
    p->room = room;
  }else{
    path_t *newp;
    if ((newp = malloc(sizeof(path_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY; 
    newp->room = room;
    newp->next = p;
    *path = newp;
  }
  return PROG_SUCCESS;
}

static int find_room_child(maze_t *maze, path_t **child, int *room){
  if (*child == NULL) return PROG_ERR_ALREADY_FREED;
  int x, y, hit = 0;
  (*child)->room = NULL;
  (*child)->next = NULL;
  for (x = 0; x < maze->width; ++x){
    for (y = 0; y < maze->height; ++y){
      if (room == &maze->room[x+y*maze->width]){
        ++hit;
        break;
      }
    }
    if (hit) break;
  }
  if (hit == 0) return PROG_ERR_UNKNOWN;
  if (x != 0 && !exists_wall(maze, x, y, x-1, y)) push_path_list(child, &maze->room[(x-1)+y*maze->width]);
  if (x != maze->width-1 && !exists_wall(maze, x, y, x+1, y)) push_path_list(child, &maze->room[(x+1)+y*maze->width]);
  if (y != 0 && !exists_wall(maze, x, y, x, y-1)) push_path_list(child, &maze->room[x+(y-1)*maze->width]);
  if (y != maze->height-1 && !exists_wall(maze, x, y, x, y+1)) push_path_list(child, &maze->room[x+(y+1)*maze->width]);
  return PROG_SUCCESS;
}

static int step_solve(maze_t *maze, path_t **path, path_t **tpath, int *read){
  path_t *top = *path;
  if (top->room == &maze->room[maze->goal]){
    *tpath = top;
    return 1;
  }else{
    int ret, hit = 0;
    path_t *child, *cp;
    if ((child = malloc(sizeof(path_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
    if ((ret = find_room_child(maze, &child, top->room)) != PROG_SUCCESS) return ret;
    if (child->room != NULL){
      for (cp = child; cp != NULL; cp = cp->next){
        int cp_flag = 0;
        if ((ret = get_read_flag(maze, read, cp->room, &cp_flag)) != PROG_SUCCESS) return ret;
        if (cp_flag == 0){
          set_read_flag(maze, read, cp->room, 1);
          if ((ret = push_path_list(path, cp->room)) != PROG_SUCCESS) return ret;
          ++hit;
          break;
        }
      }
    }
    clear_path_list(child);
    if (!hit) if ((ret = pop_path_list(path)) != PROG_SUCCESS) return ret;
  }
  return PROG_SUCCESS;
}

int run_solver(maze_t *maze, path_t **path){
  /*if (path != NULL) return PROG_ERR_NOT_FREED;*/
  if ((*path = malloc(sizeof(path_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  path_t *p = *path;
  int *read;
  if ((read = calloc(maze->width * maze->height, sizeof(int))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  p->room = &maze->room[maze->start];
  p->next = NULL;
  set_read_flag(maze, read, p->room, 1);
  while (p->room != NULL){
    int ret = step_solve(maze, &p, path, read);
    if (ret == 1) break;
    else if (ret != PROG_SUCCESS) return ret;
  }
  free(read);
  return (p->room == NULL) ? PROG_ERR_UNKNOWN : PROG_SUCCESS;
}

int run_solver_step(maze_t *maze, com_t *com){
  int ret = step_solve(maze, &com->path, &com->tpath, com->read);
  return (ret == 1) ? !ret : ret;
}

int clear_path_list(path_t *path){
  if (path == NULL) return PROG_ERR_ALREADY_FREED;
  while (path != NULL){
    path_t *next = path->next;
    free(path);
    path = next;
  }
  return PROG_SUCCESS;
}
