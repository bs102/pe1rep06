#include "common.h"
#include "util.h"

static uint32_t seed[4] = {0};

/* http://meme.biology.tohoku.ac.jp/klabo-wiki/index.php?cmd=read&page=%B7%D7%BB%BB%B5%A1%2FC%2B%2B */
void prog_srand(unsigned int s){
  int i;
  for (i = 0; i < 4; ++i) seed[i] = s = 1812433253U*(s^(s>>30))+i;
}

unsigned int prog_rand(void){
  unsigned int t = seed[0] ^ (seed[0] << 11);
  seed[0] = seed[1]; seed[1] = seed[2]; seed[2] = seed[3];
  return (seed[3] = (seed[3]^(seed[3]>>19)) ^ (t^(t>>8)));
}

