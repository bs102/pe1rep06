#include "common.h"

#include <string.h>

#include "conf.h"

int add_conf_entry(conf_t **conf, char *name, int val, int constant, int min, int max){
  conf_t *nc;
  if ((nc = malloc(sizeof(conf_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
  nc->name = name;
  nc->val = val;
  nc->constant = constant;
  nc->min = min;
  nc->max = max;
  nc->next = NULL;
  if (*conf == NULL) *conf = nc;
  else{
    conf_t *cp;
    for (cp = *conf; cp->next != NULL; cp = cp->next);
    cp->next = nc;
  }
  return PROG_SUCCESS;
}

int get_conf_entry(conf_t *conf, char *name, conf_t **out){
  if (conf == NULL) return PROG_ERR_ALREADY_FREED;
  conf_t *cp;
  for (cp = conf; cp != NULL; cp = cp->next){
    if (strcmp(name, cp->name) == 0){
      if ((*out = malloc(sizeof(conf_t))) == NULL) return PROG_ERR_FAILED_ALLOCATE_MEMORY;
      (*out)->next = NULL;
      memmove(*out, cp, sizeof(conf_t));
      return PROG_SUCCESS;
    }
  }
  return PROG_ERR_INVALID_CASE;
}

int get_conf_entry_value(conf_t *conf, char *name, int *out){
  if (conf == NULL) return PROG_ERR_ALREADY_FREED;
  int ret;
  conf_t *cp = NULL;
  if ((ret = get_conf_entry(conf, name, &cp)) != PROG_SUCCESS) return ret;
  *out = cp->val;
  free(cp);
  return PROG_SUCCESS;
}

int get_conf_entries(conf_t *conf, conf_t **out){
  if (conf == NULL) return PROG_ERR_ALREADY_FREED;
  if (*out != NULL) return PROG_ERR_NOT_FREED;
  conf_t *cp;
  for (cp = conf; cp != NULL; cp = cp->next){
    int ret;
    conf_t *nc;
    if ((ret = get_conf_entry(conf, cp->name, &nc)) != PROG_SUCCESS) return ret;
    if (*out == NULL) *out = nc;
    else{
      conf_t *cop;
      for (cop = *out; cop->next != NULL; cop = cop->next);
      cop->next = nc;
    }
  }
  return PROG_SUCCESS;
}

int set_conf_entry(conf_t *conf, char *name, int val){
  if (conf == NULL) return PROG_ERR_ALREADY_FREED;
  conf_t *cp;
  for (cp = conf; cp != NULL; cp = cp->next){
    if (!cp->constant && strcmp(cp->name, name) == 0){
      cp->val = val;
      return PROG_SUCCESS;
    }
  }
  return PROG_ERR_INVALID_CASE;
}

int clear_conf(conf_t *conf){
  if (conf == NULL) return PROG_ERR_ALREADY_FREED;
  while(conf != NULL){
    conf_t *next = conf->next;
    free(conf);
    conf = next;
  }
  return PROG_SUCCESS;
}
